//
//  constants.h
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#ifndef Monsoon_Menu_constants_h
#define Monsoon_Menu_constants_h


// constants
static NSString * kMMBatchSize = @"MMBatchSize";
static NSString * kMMflavorType = @"MMflavorType";
static NSString * kMMflavorSpiciness = @"MMflavorSpiciness";
static NSString * kMMflavorTexture = @"MMflavorTexture";
static NSString * kMMflavorDegree = @"MMflavorDegree";
static NSString * kMMMeal = @"MMMeal";
static NSString * kColorThemeDark = @"#2f3440";
static NSString * kColorStateDefault = @"#5c0528";
static NSString * kColorStateSelected = @"#f66864";

// flags
#define BACKGROUND_MOVES_ON_SEGUE    YES

#endif
