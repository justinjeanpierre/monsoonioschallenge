//
//  AppUtilities.h
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppUtilities : NSObject

/*
 *  @param hexString The string to be converted to a UIColor object.  Optionally contains leading # character.
 *  @return A UIColor object.
 */
+ (UIColor *) colorFromHexString:(NSString *)hexString;

@end
