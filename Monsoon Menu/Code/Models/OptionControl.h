//
//  OptionControl.h
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-05-02.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionControl : UIControl {
    NSArray *optionsArray;
    NSString *selectedOption;
}

// An array of options to present to the user. Each option should be a string.
@property (nonatomic, strong) NSArray *optionsArray;
// This is the option that was chosen by the user.  Defaults to the first object in optionsArray;
@property (nonatomic, strong) NSString *selectedOption;

/*
 * The control will select an option at random
 * from those provided in optionsArray.
 */
- (void)displayRandomOption;


@end
