//
//  Meal.m
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import "Meal.h"

@implementation Meal

@synthesize batchSize;
@synthesize flavorType;
@synthesize flavorSpiciness;
@synthesize flavorTexture;
@synthesize flavourDegree;
@synthesize mealType;

-(id)initWithBatchSize:(NSString *)bSize
            flavorType:(NSString *)fType
       flavorSpiciness:(NSString *)fSpiciness
         flavorTexture:(NSString *)fTexture
         flavourDegree:(NSString *)fDegree
              mealType:(NSString *)mType {
    self = [super init];
    
    if (self) {
        if (bSize) {
            self.batchSize = bSize;
        } else {
            self.batchSize = @"no batch size specified";
        }
        
        if (fType) {
            self.flavorType = fType;
        } else {
            self.flavorType = @"no flavor type specified";
        }
        
        if (fSpiciness) {
            self.flavorSpiciness = fSpiciness;
        } else {
            self.flavorSpiciness = @"no spice preference specified";
        }
        
        if (fTexture) {
            self.flavorTexture = fTexture;
        } else {
            self.flavorTexture = @"no texture specified";
        }
        
        if (fDegree) {
            self.flavourDegree = fDegree;
        } else {
            self.flavourDegree = @"no portion size specified";
        }
        
        if (mType) {
            self.mealType = mType;
        } else {
            self.mealType = @"no meal type  specified";
        }
    }
    
    return self;
}

@end
