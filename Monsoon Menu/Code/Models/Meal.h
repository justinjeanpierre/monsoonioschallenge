//
//  Meal.h
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Meal : NSObject
{
    NSString *batchSize;
    NSString *flavorType;
    NSString *flavorSpiciness;
    NSString *flavorTexture;
    NSString *flavourDegree;
    NSString *mealType;
}

@property (nonatomic, strong) NSString *batchSize;
@property (nonatomic, strong) NSString *flavorType;
@property (nonatomic, strong) NSString *flavorSpiciness;
@property (nonatomic, strong) NSString *flavorTexture;
@property (nonatomic, strong) NSString *flavourDegree;
@property (nonatomic, strong) NSString *mealType;


/*
 * @param bSize Food uniqueness
 * @param fType Desired flavor
 * @param fSpiciness How spicy meal should be
 * @param fTexture User's preferred texture
 * @param fDegree Portion size (Or how spicy? Sweet?)
 * @param mType User's intended meal.
 */
- (id)initWithBatchSize:(NSString *)bSize
             flavorType:(NSString *)fType
        flavorSpiciness:(NSString *)fSpiciness
          flavorTexture:(NSString *)fTexture
          flavourDegree:(NSString *)fDegree
               mealType:(NSString *)mType;
@end
