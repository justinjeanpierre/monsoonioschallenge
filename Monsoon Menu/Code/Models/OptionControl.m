//
//  OptionControl.m
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-05-02.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#define DEGREES_SPACER  12.0f

#import "OptionControl.h"
#import "AppUtilities.h"
#import "constants.h"

@interface OptionControl ()

@property (nonatomic, readwrite) int tapcount;
@property (nonatomic, strong) UIImageView *indicatorView;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation OptionControl

@synthesize tapcount;
@synthesize indicatorView;

@synthesize optionsArray;
@synthesize selectedOption;
@synthesize titleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    float frameWidth = CGRectGetWidth(self.frame);
    float frameHeight = CGRectGetHeight(self.frame);
    
    // create solid circle background
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake((0.05 * frameWidth), (0.05 * frameHeight), (0.9 * frameWidth), (0.9 * frameHeight))];
    // (made background 10% smaller than frame to allow space for indicator rings)
    backgroundImageView.layer.cornerRadius = (0.9*frameWidth)/2; // make the background into a circle
    backgroundImageView.alpha = 0.2f;
    backgroundImageView.backgroundColor = [UIColor blackColor];
    [self addSubview:backgroundImageView];

    
    // draw indicator
    [self configureIndicatorView];
    // create label
    float labelHeight = 22.0f;
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.05 * CGRectGetWidth(self.bounds),
                                                                CGRectGetMidY(self.bounds) - (0.5 * labelHeight),
                                                                0.9 * CGRectGetWidth(self.bounds),
                                                                labelHeight)];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLabel];
    self.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:16.0f];
    self.titleLabel.textColor = [UIColor whiteColor];
    // Not sure what font was used.  This appears to be the closest, but the K doesn't look right.
    // Other potentials were Hiragino Kaku Gothic ProN, Geeza Pro, and DIN Alternate.
    
    self.titleLabel.text = [[optionsArray objectAtIndex:0] uppercaseString];
    self.selectedOption = [optionsArray objectAtIndex:0];
}

-(void)awakeFromNib {
    [self addTarget:self action:@selector(didPressButton) forControlEvents:UIControlEventTouchUpInside];
    [self setBackgroundColor:[UIColor clearColor]];
}

-(IBAction)didPressButton {
    
    tapcount++;
    
    if (tapcount == optionsArray.count) {
        tapcount = 0;
    }
    
    // rotate the indicatorView
    float degreesToRotate = (360/optionsArray.count) * tapcount;
    
    CGAffineTransform rotationTransform = CGAffineTransformRotate(CGAffineTransformIdentity, [self radiansFromDegrees:degreesToRotate]);
    // animate the rotation
    [UIView beginAnimations:@"rotateIndicatorAnimation" context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.33f];
    self.indicatorView.transform = rotationTransform;
    [UIView commitAnimations];
    
    // update the title
    self.titleLabel.text = [[optionsArray objectAtIndex:tapcount] uppercaseString];
    self.selectedOption = [optionsArray objectAtIndex:tapcount];
    
}

#pragma mark - Custom drawing

-(void)configureIndicatorView {
    self.indicatorView = [[UIImageView alloc] initWithFrame:self.bounds];
    
    float lineWidth = 1.0f;
    float frameWidth = CGRectGetWidth(self.indicatorView.frame);
    float frameHeight = CGRectGetHeight(self.indicatorView.frame);
    float radius = (frameWidth / 2) - 4 * lineWidth;
    CGPoint arcStartPoint = CGPointMake(radius, CGRectGetHeight(self.indicatorView.frame) - lineWidth); // start at bottom of circle
    CGPoint arcCenter = CGPointMake(frameWidth/2, frameHeight/2);
    
    // create image context. same size as indicator view
    UIGraphicsBeginImageContext(self.indicatorView.frame.size);
    // set the image context as context we will be working with
    CGContextRef context = UIGraphicsGetCurrentContext();
    // set up the line appearance
    CGContextSetLineWidth(context, lineWidth);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    // start line
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, nil, arcStartPoint.x, arcStartPoint.y);
    
    // set aside 10 (value of DEGREES_SPACER) degrees per option for space between arcs
    CGFloat segmentDegrees = (360 - (DEGREES_SPACER * optionsArray.count))/optionsArray.count;
    
    // specify starting angle
    CGFloat startAngle = 90.0f - (0.5 * DEGREES_SPACER);
    CGFloat endAngle;// = 0.0f; // value not important right now, just making sure it is initialized to something
    
    // draw arc for each option
    for (int i = 0; i < optionsArray.count; i++) {
        if (i == 0) { // make first segment "selected" color
            CGContextSetStrokeColorWithColor(context, [AppUtilities colorFromHexString:kColorStateSelected].CGColor);
        } else {
            CGContextSetStrokeColorWithColor(context, [AppUtilities colorFromHexString:kColorStateDefault].CGColor);
        }
        
        endAngle = startAngle + segmentDegrees + DEGREES_SPACER;
        // create color arcs
        CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, [self radiansFromDegrees:startAngle + DEGREES_SPACER], [self radiansFromDegrees:endAngle], 0);
        CGContextStrokePath(context);
        
        startAngle = endAngle;
        // create blank arcs between indicator sections
        CGContextAddArc(context, arcCenter.x, arcCenter.y, radius, [self radiansFromDegrees:startAngle], [self radiansFromDegrees:endAngle], 0);
        CGContextSetStrokeColorWithColor(context, [UIColor clearColor].CGColor);
        CGContextStrokePath(context);
    }
    
    // make an image from the paths
    CGImageRef imgref = CGBitmapContextCreateImage(context);
    UIImage *img = [UIImage imageWithCGImage:imgref];
    
    CFRelease(imgref);
    CFRelease(path);
    
    self.indicatorView.image = img;
    [self addSubview:self.indicatorView];
}

#pragma mark - random

-(void)displayRandomOption {
    // play a sound
    
    int randomNumber = arc4random() % optionsArray.count;
    
    self.tapcount = --randomNumber;
    
    [self didPressButton];
}

#pragma mark - helpers

-(float)radiansFromDegrees:(float)angleInDegrees {
    // there is probably already a simpler way to do this
    return (angleInDegrees/360) * 2 * M_PI;
}

@end
