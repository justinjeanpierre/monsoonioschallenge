//
//  ResultViewController.h
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Meal.h"

@interface ResultViewController : UIViewController {
    Meal *meal;
}

@property (nonatomic, strong) Meal *meal;

@end
