//
//  ResultViewController.m
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#define LABEL_CORNER_RADIUS 12.0f;
#define KEY_SOUND_ENABLED   @"sound_enabled"

#import "ResultViewController.h"
#import "AppUtilities.h"
#import "constants.h"

@interface ResultViewController ()

@property (weak, nonatomic) IBOutlet UILabel *batchSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *flavorTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *flavorSpicinessLabel;
@property (weak, nonatomic) IBOutlet UILabel *flavorTextureLabel;
@property (weak, nonatomic) IBOutlet UILabel *flavorDegreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *mealTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *soundStateLabel;
@property (weak, nonatomic) IBOutlet UISwitch *soundStateToggleSwitch;

-(IBAction)didToggleSound:(UISwitch *)sender;

@end

@implementation ResultViewController

@synthesize meal;

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // configure the navigationbar/toolbar
    self.navigationController.navigationBar.tintColor = [AppUtilities colorFromHexString:kColorThemeDark];
    self.navigationController.navigationBarHidden = YES;
   
    // put the labels into an array for easier manipulation
    NSArray *labelsArray = @[ self.batchSizeLabel, self.flavorTypeLabel, self.flavorSpicinessLabel, self.flavorTextureLabel, self.flavorDegreeLabel, self.mealTypeLabel];
    
    for (UILabel *selectedOptionLabel in labelsArray) {
        selectedOptionLabel.layer.cornerRadius = LABEL_CORNER_RADIUS;
        selectedOptionLabel.layer.borderWidth = 1.0f;
        selectedOptionLabel.layer.borderColor = [AppUtilities colorFromHexString:kColorStateDefault].CGColor;
    }
    
    // populate labels
    self.batchSizeLabel.text = self.meal.batchSize;
    self.flavorTypeLabel.text =  self.meal.flavorType;
    self.flavorSpicinessLabel.text =  self.meal.flavorSpiciness;
    self.flavorTextureLabel.text =  self.meal.flavorTexture;
    self.flavorDegreeLabel.text =  self.meal.flavourDegree;
    self.mealTypeLabel.text =  self.meal.mealType;

    if (BACKGROUND_MOVES_ON_SEGUE) {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MON_Rectangle-5.png"]]];
    } else {
        [self.navigationController.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MON_Rectangle-5.png"]]];
    }
    
    // set the sound toggle switch to saved state, will default to "off"
    [self.soundStateToggleSwitch setOnTintColor:[AppUtilities colorFromHexString:kColorStateSelected]];
    [self.soundStateToggleSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:KEY_SOUND_ENABLED] animated:NO];
    
    self.soundStateLabel.textColor = [AppUtilities colorFromHexString:kColorStateDefault];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:KEY_SOUND_ENABLED] == NO) {
        self.soundStateLabel.text = @"sound is off";
    } else {
        self.soundStateLabel.text = @"sound is on";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sound

-(void)didToggleSound:(UISwitch *)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (sender.isOn) {
        self.soundStateLabel.text = @"sound is on";
        [defaults setBool:YES forKey:KEY_SOUND_ENABLED];
    } else {
        self.soundStateLabel.text = @"sound is off";
        [defaults setBool:NO forKey:KEY_SOUND_ENABLED];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}

@end
