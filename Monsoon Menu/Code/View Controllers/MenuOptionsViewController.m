//
//  MenuOptionsViewController.m
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import "MenuOptionsViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AppUtilities.h"
#import "constants.h"
#import "Meal.h"
#import "ResultViewController.h"
#import "OptionControl.h"

@interface MenuOptionsViewController ()

@property (weak, nonatomic) IBOutlet OptionControl *batchSizeControl;
@property (weak, nonatomic) IBOutlet OptionControl *flavorTypeControl;
@property (weak, nonatomic) IBOutlet OptionControl *flavorTextureControl;
@property (weak, nonatomic) IBOutlet OptionControl *flavorSpicinessControl;
@property (weak, nonatomic) IBOutlet OptionControl *flavorDegreeControl;
@property (weak, nonatomic) IBOutlet OptionControl *mealControl;

@property (weak, nonatomic) IBOutlet UIToolbar *menuToolbar;

@property (readwrite) CFURLRef soundURLRef;
@property (readonly) SystemSoundID soundEffectObject;

- (IBAction)didPressGoButton:(UIButton *)sender;
- (IBAction)didPressShuffleButton:(UIButton *)sender;

@end

@implementation MenuOptionsViewController

#pragma mark - configuration - UINavigationBar

-(void)configureButtonBar {
    // make navigation bar transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBarHidden = YES;

    UIBarButtonItem *fixedSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                                             target:nil
                                                                                             action:nil];
    fixedSpaceBarButtonItem.width = 12.0f;
    
    UIBarButtonItem *flexibleSpaceBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                                                target:nil
                                                                                                action:nil];
    
    UIBarButtonItem *menuBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MON_menuIcon.png"]
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:nil];
    
    UIBarButtonItem *calendarBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MON_calendarIcon.png"]
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:self
                                                                             action:nil];
    
    UIBarButtonItem *compassBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MON_compassIcon.png"]
                                                                             style:UIBarButtonItemStyleDone
                                                                            target:self
                                                                            action:nil];
    
    UIBarButtonItem *searchBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MON_searchIcon.png"]
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:nil];

    // change button colour
    [menuBarButtonItem setTintColor:[AppUtilities colorFromHexString:kColorThemeDark]];
    [calendarBarButtonItem setTintColor:[AppUtilities colorFromHexString:kColorThemeDark]];
    [compassBarButtonItem setTintColor:[AppUtilities colorFromHexString:kColorThemeDark]];
    [searchBarButtonItem setTintColor:[AppUtilities colorFromHexString:kColorThemeDark]];
    
    // I had originally added the UIBarButtonItems to a UINavigationBar,
    // but the spacing didn't match the supplied mock-up (too much space between search, calendar, and compass buttons.)
    // Using a UIToolbar instead solved this, only now the appearance/disappearance of the back button (after tapping the GO button) does not look natural.
    // Since the goal of the challenge was "pixel-perfect" recreation,
    // I chose to trade sub-optimal behavior in the optional part of the exercise for accurate appearance in the mandatory part.
    self.menuToolbar.tintColor = [UIColor clearColor];
    [self.menuToolbar setBackgroundImage:[UIImage new] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [self.menuToolbar setShadowImage:[UIImage new] forToolbarPosition:UIBarPositionAny];
    self.menuToolbar.items = @[ searchBarButtonItem, fixedSpaceBarButtonItem, calendarBarButtonItem, fixedSpaceBarButtonItem, compassBarButtonItem, flexibleSpaceBarButtonItem, menuBarButtonItem ];

    // Setting the supplied image as the background for the individual view controllers looked strange.
    // I think it looks better if only the overlayed contents move while the background stays in place.
    //
    // This following code sets the background of the navigation controller to the supplied image.
    // To disable this "effect", set the BACKGROUND_MOVES_ON_SEGUE flag in constants.h to NO;
    if (BACKGROUND_MOVES_ON_SEGUE) {
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MON_Rectangle-5.png"]]];
    } else {
        [self.navigationController.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"MON_Rectangle-5.png"]]];
    }
}

-(void)configureMenuOptionButtons {
    // load options array from plist
    NSDictionary *optionsPlistDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"options" ofType:@"plist"]];

    self.batchSizeControl.optionsArray = [optionsPlistDictionary objectForKey:kMMBatchSize];
    self.flavorTypeControl.optionsArray = [optionsPlistDictionary objectForKey:kMMflavorType];
    self.flavorSpicinessControl.optionsArray = [optionsPlistDictionary objectForKey:kMMflavorSpiciness];
    self.flavorTextureControl.optionsArray = [optionsPlistDictionary objectForKey:kMMflavorTexture];
    self.flavorDegreeControl.optionsArray = [optionsPlistDictionary objectForKey:kMMflavorDegree];
    self.mealControl.optionsArray = [optionsPlistDictionary objectForKey:kMMMeal];
}

-(void)configureSound {
    NSURL *shuffleSound = [[NSBundle mainBundle] URLForResource:@"button-33a" withExtension:@"wav"];
    self.soundURLRef = (__bridge CFURLRef)[shuffleSound copy];
    AudioServicesCreateSystemSoundID(self.soundURLRef, &_soundEffectObject);
}

#pragma mark - Object

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configureButtonBar];
    [self configureMenuOptionButtons];
    [self configureSound];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;         // dark status bar text, icons
//    return UIStatusBarStyleLightContent;    // light status bar text, icons
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"menuDetailSegue"]) {
        ResultViewController *resultViewController = (ResultViewController *)[segue destinationViewController];
        [resultViewController setMeal:(Meal *)sender];

        if (!BACKGROUND_MOVES_ON_SEGUE) {
            // The default UINavigationController behaviour looks a little weird;
            // the current view stays visible for a little to long once the next view is visible.
            // This is intended to make it fade a little faster.
            [UIView beginAnimations:@"fadeView" context:NULL];
            [UIView setAnimationDuration:0.33f];
            [self.view setAlpha:0.0f];
            [UIView commitAnimations];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    
    if (!BACKGROUND_MOVES_ON_SEGUE) {
        // Since we made this controller's view disappear in -prepareForSegue:sender,
        // we need to make it reappear when resultViewController is popped.
        [UIView beginAnimations:@"showView" context:NULL];
        [UIView setAnimationDuration:0.15f];
        [self.view setAlpha:1];
        [UIView commitAnimations];
    }
}

- (IBAction)didPressGoButton:(UIButton *)sender {
    Meal *meal = [[Meal alloc] initWithBatchSize:self.batchSizeControl.selectedOption
                                      flavorType:self.flavorTypeControl.selectedOption
                                 flavorSpiciness:self.flavorSpicinessControl.selectedOption
                                   flavorTexture:self.flavorTextureControl.selectedOption
                                   flavourDegree:self.flavorDegreeControl.selectedOption
                                        mealType:self.mealControl.selectedOption];
    
    [self performSegueWithIdentifier:@"menuDetailSegue" sender:meal];
}

- (IBAction)didPressShuffleButton:(UIButton *)sender {
    [self.batchSizeControl displayRandomOption];
    [self.flavorTypeControl displayRandomOption];
    [self.flavorSpicinessControl displayRandomOption];
    [self.flavorTextureControl displayRandomOption];
    [self.flavorDegreeControl displayRandomOption];
    [self.mealControl displayRandomOption];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sound_enabled"]) {
        AudioServicesPlaySystemSound(_soundEffectObject);
    }
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (motion == UIEventSubtypeMotionShake) {
        [self didPressShuffleButton:nil];
    }
}

@end
