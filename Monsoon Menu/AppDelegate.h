//
//  AppDelegate.h
//  Monsoon Menu
//
//  Created by Justin Jean-Pierre on 2014-04-29.
//  Copyright (c) 2014 Jean-Pierre Digital Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
